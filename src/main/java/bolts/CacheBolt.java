package bolts;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import org.apache.log4j.Logger;
import util.FName;
import util.OrderItem;
import util.StreamId;

import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;


/**
 * 缓存数据并弹出6分钟前的数据
 * Created by linwanying on 2016/4/11.
 */
public class CacheBolt extends BaseRichBolt {

    static Logger log = Logger.getLogger(CacheBolt.class);

    private LinkedBlockingQueue<OrderItem> data = null;
    private OutputCollector collector;

    @SuppressWarnings("rawtypes")
    public void prepare(Map config, TopologyContext context, OutputCollector collector) {
        this.data = new LinkedBlockingQueue<OrderItem>();
        this.collector = collector;
        log.info("order CacheBolt:");
    }

    public void execute(Tuple tuple) {
        long recordTime = tuple.getLongByField(FName.RECORDTIME.name());
        String msisdn = tuple.getStringByField(FName.MSISDN.name());
        String terminal = tuple.getStringByField(FName.TERMINAL.name());
        String platform = tuple.getStringByField(FName.PLATFORM.name());
        String orderType = tuple.getStringByField(FName.ORDERTYPE.name());
        String productID = tuple.getStringByField(FName.PRODUCTID.name());
        String bookID = tuple.getStringByField(FName.BOOKID.name());
        String chapterID = tuple.getStringByField(FName.CHAPTERID.name());
        String channelCode = tuple.getStringByField(FName.CHANNELCODE.name());
        String cost = tuple.getStringByField(FName.COST.name());
        String provinceId = tuple.getStringByField(FName.PROVINCEID.name());
        String wapIp = tuple.getStringByField(FName.WAPIP.name());
        String sessionId = tuple.getStringByField(FName.SESSIONID.name());
        String promotionid = tuple.getStringByField(FName.PROMOTIONID.name());

        OrderItem item = new OrderItem(msisdn, recordTime, terminal, platform, orderType, productID,
                bookID, chapterID, channelCode, cost, provinceId, wapIp, sessionId, promotionid);
        try {
            data.put(item);
        } catch (InterruptedException e) {

        }
        for (OrderItem oi : data) {
            log.info("queue.Msisdn: " + oi.getMsisdn());
        }
        log.info("timelag: " + Long.toString(recordTime - data.peek().getRecordTime()));

        while (data.peek() != null && item.getRecordTime() - data.peek().getRecordTime() > 360000) {
            OrderItem orderItem = data.poll();
            log.info("data.poll(): " + orderItem.getMsisdn());
            collector.emit(StreamId.DATASTREAM.name(), new Values(orderItem.getMsisdn(),
                    orderItem.getRecordTime(), orderItem.getTerminal(), orderItem.getPlatform(),
                    orderItem.getOrderType(), orderItem.getProductID(), orderItem.getBookID(),
                    orderItem.getChapterID(), orderItem.getChannelCode(), orderItem.getCost(),
                    orderItem.getProvinceId(), orderItem.getWapIp(), orderItem.getSessionId(),
                    orderItem.getPromotionid()));
        }

    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(StreamId.DATASTREAM.name(),
                new Fields(FName.MSISDN.name(), FName.RECORDTIME.name(),
                        FName.TERMINAL.name(), FName.PLATFORM.name(),
                        FName.ORDERTYPE.name(), FName.PRODUCTID.name(),
                        FName.BOOKID.name(), FName.CHAPTERID.name(),
                        FName.CHANNELCODE.name(), FName.COST.name(),
                        FName.PROVINCEID.name(), FName.WAPIP.name(),
                        FName.SESSIONID.name(), FName.PROMOTIONID.name()));
    }

    public void cleanup() {

    }
}
