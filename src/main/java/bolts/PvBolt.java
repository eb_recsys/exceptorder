package bolts;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;
import util.FName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 完成对pv数据的入库操作(入库时删除一小时5分钟前的数据)
 * 入hbase库表格：pv_info
 * 入库字段格式：recordTime1|recordTime2|...
 * Created by linwanying on 2016/4/11.
 */
public class PvBolt extends BaseRichBolt {
    static Logger log = Logger.getLogger(PvBolt.class);
    private HTable table;

    @SuppressWarnings("rawtypes")
    public void prepare(Map config, TopologyContext context, OutputCollector collector) {
        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "10.1.69.170");
        conf.set("hbase.zookeeper.property.clientPort", "2182");
        try {
            table = new HTable(conf, Bytes.toBytes("pv_info"));
            table.setAutoFlushTo(false);
        } catch (IOException e) {
            //DO NOTHING
        }
    }

    public void execute(Tuple tuple) {
        log.info("pvbolt:123456789");
        Long recordTime = tuple.getLongByField(FName.RECORDTIME.name());
        String msisdn = tuple.getStringByField(FName.MSISDN.name());
        String bookid = tuple.getStringByField(FName.BOOKID.name());
        String chapterid = tuple.getStringByField(FName.CHAPTERID.name());
        String bookidChapterid = chapterid.isEmpty() ? bookid : chapterid;
        String time = Long.toString(recordTime);

        String msisdn_bookid = msisdn + "_" + bookidChapterid;
        Get get = new Get(Bytes.toBytes(msisdn_bookid));
        Put put = new Put(Bytes.toBytes(msisdn_bookid));
        Result result = null;
        try {
            result = table.get(get);
        } catch (IOException e) {
            //TO DO
        }
        String info;
        String[] data1;
        if (!result.isEmpty()) {
            info = Bytes.toString(result.getValue(
                    Bytes.toBytes("cf"), Bytes.toBytes("info")));
            data1 = info.split("\\|");
            List<String> list = new ArrayList<String>();
            for (String s : data1) {
                list.add(s);
            }
            //删除过期数据
            Iterator<String> it = list.iterator();
            while( it.hasNext()){
                String cur = it.next();
                if(list.size() != 0 && recordTime - Long.parseLong(cur) > 3900000){
                    it.remove();
                }
            }
            info = "";
            for (int j = 0; j < list.size(); j++) {
                info = info + list.get(j) + "|";
            }
            put.add(Bytes.toBytes("cf"), Bytes.toBytes("info"),
                    Bytes.toBytes(info + time));
        } else {
            put.add(Bytes.toBytes("cf"), Bytes.toBytes("info"),
                    Bytes.toBytes(time));
        }

        try {
            log.info(time);
            table.put(put);
            table.flushCommits();
        } catch (IOException e) {
            //TO DO
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }

    public void cleanup() {

    }
}
