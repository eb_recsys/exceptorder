package bolts;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import util.*;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 完成1~12条规则的判断
 * ordertable的key: msisdn_channelcode
 * ordertable的value格式：ordertype1,bookid1,chapterid1,fee1,
 * recordtime1,sessionID1,productID1,provinceID1,rule1~12|...
 * Created by linwanying on 2016/4/26.
 */
public class RuleBolt extends BaseRichBolt {
    private HTable ordertable;
    private HTable pvtable;
    private ConcurrentHashMap<String, TimeCacheList> channelcodemap = null;
    private ConcurrentHashMap<String, TimeCacheList> sessionIDmap = null;
    private ConcurrentHashMap<String, TimeCacheList> wapIpmap = null;
    private ConcurrentHashMap<String, TimeCacheList> terminalmap = null;
    private OutputCollector collector;

    @SuppressWarnings("rawtypes")
    public void prepare(Map config, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        channelcodemap = new ConcurrentHashMap<String, TimeCacheList>();
        sessionIDmap = new ConcurrentHashMap<String, TimeCacheList>();
        wapIpmap = new ConcurrentHashMap<String, TimeCacheList>();
        terminalmap = new ConcurrentHashMap<String, TimeCacheList>();

        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "10.1.69.170");
        conf.set("hbase.zookeeper.property.clientPort", "2182");
        try {
            ordertable = new HTable(conf, Bytes.toBytes("order_info"));
            pvtable = new HTable(conf, Bytes.toBytes("pv_info"));
        } catch (IOException e) {
            //TO DO
        }
    }

    public void execute(Tuple tuple) {
        int count = 10;
        long recordTime = tuple.getLongByField(FName.RECORDTIME.name());
        String msisdn = tuple.getStringByField(FName.MSISDN.name());
        String terminal = tuple.getStringByField(FName.TERMINAL.name());
        String platform = tuple.getStringByField(FName.PLATFORM.name());
        String orderType = tuple.getStringByField(FName.ORDERTYPE.name());
        String productID = tuple.getStringByField(FName.PRODUCTID.name());
        String bookID = tuple.getStringByField(FName.BOOKID.name());
        String chapterID = tuple.getStringByField(FName.CHAPTERID.name());
        String channelCode = tuple.getStringByField(FName.CHANNELCODE.name());
        String cost = tuple.getStringByField(FName.COST.name());
        String provinceId = tuple.getStringByField(FName.PROVINCEID.name());
        String wapIp = tuple.getStringByField(FName.WAPIP.name());
        String sessionId = tuple.getStringByField(FName.SESSIONID.name());
        String promotionid = tuple.getStringByField(FName.PROMOTIONID.name());
        String srecordtime = Long.toString(recordTime);
        String bookidChapterid = chapterID.isEmpty() ? bookID : chapterID;
        String msisdn_bookid = msisdn + "_" + bookidChapterid;
        String msisdn_channelcode = msisdn + "_" + channelCode;
        StringBuffer rule = new StringBuffer("111111111111");
        TimeBaseItem channelcodeItem = new TimeBaseItem(channelCode, recordTime);
        TimeBaseItem sessionItem = new TimeBaseItem(sessionId, recordTime);
        TimeBaseItem wapIpItem = new TimeBaseItem(wapIp, recordTime);
        TimeBaseItem terminalItem = new TimeBaseItem(terminal, recordTime);
        OrderItem orderItem = new OrderItem(msisdn, recordTime, terminal, platform, orderType, productID,
                bookID, chapterID, channelCode, cost, provinceId, wapIp, sessionId, promotionid);
        if (!(orderType.equals("4") || orderType.equals("5") || orderType.equals("9") || orderType.equals("99"))) {
            count = RuleJudgement.pvCount(msisdn_bookid, pvtable, recordTime, 3600000, -300000);
            if (count == 0) {
                rule.setCharAt(0,'0');
            }
            if (count == 1) {
                rule.setCharAt(1,'0');
            }
            if (count > 1 && count <= 5) {
                rule.setCharAt(2,'0');
            }
        }
        int channelsize = RuleJudgement.rule4_9_10_11(channelcodemap, channelcodeItem, orderItem, "4");
        if (channelsize >= 3) {
            rule.setCharAt(3,'0');
        }
        if (RuleJudgement.rule5(orderItem, ordertable)) {
            rule.setCharAt(4,'0');
        }
        if (RuleJudgement.rule6(msisdn, orderType, recordTime, ordertable)) {
            rule.setCharAt(5,'0');
        }
        if (RuleJudgement.rule7_8(recordTime, msisdn, "7", pvtable, ordertable)) {
            rule.setCharAt(6,'0');
        }
        if (RuleJudgement.rule7_8(recordTime, msisdn, "8", pvtable, ordertable)) {
            rule.setCharAt(7,'0');
        }
        int sessionsize = RuleJudgement.rule4_9_10_11(sessionIDmap, sessionItem, orderItem, "9");
        if (sessionsize >= 3) {
            rule.setCharAt(8,'0');
        }
        int wapsize = RuleJudgement.rule4_9_10_11(wapIpmap, wapIpItem, orderItem, "10");
        if (wapsize >= 3) {
            rule.setCharAt(9,'0');
        }
        int terminalsize = RuleJudgement.rule4_9_10_11(terminalmap, terminalItem, orderItem, "11");
        if (terminalsize >= 2) {
            rule.setCharAt(10,'0');
        }
        if (orderType.equals("4") && !sessionId.isEmpty() && !platform.equals("6")) {
            count = RuleJudgement.pvCount(msisdn_bookid, pvtable, recordTime, 3600000, -300000);
            if (count == 0) {
                rule.setCharAt(11,'0');
            }
        }
        Get get = new Get(Bytes.toBytes(msisdn_channelcode));
        Put put = new Put(Bytes.toBytes(msisdn_channelcode));
        Result result = null;
        try {
            result = ordertable.get(get);
        } catch (IOException e) {
            //DO NOTHING
        }
        String info = null;
        String addstr = orderType + "," + bookID + "," + chapterID + "," + cost + "," + srecordtime +
                  ","  + sessionId + "," + productID + "," + provinceId + "," + rule.toString();
        if (!result.isEmpty()) {
            info = Bytes.toString(result.getValue(
                    Bytes.toBytes("cf"), Bytes.toBytes("info")));
            put.add(Bytes.toBytes("cf"), Bytes.toBytes("info"),
                    Bytes.toBytes(info + "|" + addstr));
        } else {
            put.add(Bytes.toBytes("cf"), Bytes.toBytes("info"),
                    Bytes.toBytes(addstr));
        }
        try {
            ordertable.put(put);
        } catch (InterruptedIOException e) {
            //DO NOTHING
        } catch (RetriesExhaustedWithDetailsException e) {
            //DO NOTHING
        }
        collector.emit(StreamId.DATASTREAM2.name(), new Values(msisdn, recordTime,
                terminal, platform, orderType, productID, bookID, chapterID, channelCode,
                cost, provinceId, wapIp, sessionId, promotionid, rule.toString()));
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(StreamId.DATASTREAM2.name(),
                new Fields(FName.MSISDN.name(), FName.RECORDTIME.name(),
                        FName.TERMINAL.name(), FName.PLATFORM.name(),
                        FName.ORDERTYPE.name(), FName.PRODUCTID.name(),
                        FName.BOOKID.name(), FName.CHAPTERID.name(),
                        FName.CHANNELCODE.name(), FName.COST.name(),
                        FName.PROVINCEID.name(), FName.WAPIP.name(),
                        FName.SESSIONID.name(), FName.PROMOTIONID.name(), FName.RULE.name()));
    }

    public void cleanup() {

    }
}
