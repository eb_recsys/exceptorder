package bolts;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;
import util.FName;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Map;

/**
 * 分渠道，分产品，分省份统计异常费用和总费用
 * Created by linwanying on 2016/4/28.
 */
public class StatisticsBolt extends BaseRichBolt {
    static Logger log = Logger.getLogger(StatisticsBolt.class);
    private HTable channeltable;
    private HTable producttable;

    @SuppressWarnings("rawtypes")
    public void prepare(Map config, TopologyContext context, OutputCollector collector) {
        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "10.1.69.170");
        conf.set("hbase.zookeeper.property.clientPort", "2182");
        try {
            channeltable = new HTable(conf, Bytes.toBytes("channel_fee"));
            producttable = new HTable(conf, Bytes.toBytes("product_fee"));
        } catch (IOException e) {
            //DO NOTHING
        }
    }

    public void execute(Tuple tuple) {
        int abchanneltotal, channeltotal;
        String cost = tuple.getStringByField(FName.COST.name());
        String rule = tuple.getStringByField(FName.RULE.name());
        String msisdn = tuple.getStringByField(FName.MSISDN.name());
        String channelCode = tuple.getStringByField(FName.CHANNELCODE.name());
        String productid = tuple.getStringByField(FName.PRODUCTID.name());
        String provinceid = tuple.getStringByField(FName.PROVINCEID.name());
        String product_province = productid + "," + provinceid;
        String abchannelfee, channelfee, productfee;

        Get productget = new Get(Bytes.toBytes(product_province));
        Get channelget = new Get(Bytes.toBytes(channelCode + "_total"));
        Get abchannelget = new Get(Bytes.toBytes(channelCode + "_abnormal"));
        Put productput = new Put(Bytes.toBytes(product_province));
        Put channelput = new Put(Bytes.toBytes(channelCode + "_total"));
        Put abchannelput = new Put(Bytes.toBytes(channelCode + "_abnormal"));
        Result channelresult = null;
        Result abchannelresult = null;
        Result productresult = null;
        try {
            channelresult = channeltable.get(channelget);
            abchannelresult = channeltable.get(abchannelget);
            productresult = producttable.get(productget);
        } catch (IOException e) {
            //DO NOTHING
        }

        if (!channelresult.isEmpty()) {
            channelfee = Bytes.toString(channelresult.getValue(Bytes.toBytes("cf"), Bytes.toBytes("fee")));
            channeltotal = Integer.parseInt(channelfee) + Integer.parseInt(cost);
        } else {
            channeltotal = Integer.parseInt(cost);
        }
        channelput.add(Bytes.toBytes("cf"), Bytes.toBytes("fee"),
                Bytes.toBytes(Integer.toString(channeltotal)));
        try {
            channeltable.put(channelput);
        } catch (InterruptedIOException e) {
            //
        } catch (RetriesExhaustedWithDetailsException e) {
            //
        }

        if (!rule.isEmpty()) {
            log.info("msisdn:" + msisdn);
            log.info("rule: " + rule);
            if (!abchannelresult.isEmpty()) {
                abchannelfee = Bytes.toString(abchannelresult.getValue(Bytes.toBytes("cf"),
                        Bytes.toBytes("fee")));
                abchanneltotal = Integer.parseInt(abchannelfee) + Integer.parseInt(cost);
            } else {
                abchanneltotal = Integer.parseInt(cost);
            }
            log.info("abchanneltotal: " + abchanneltotal);
            abchannelput.add(Bytes.toBytes("cf"), Bytes.toBytes("fee"),
                    Bytes.toBytes(Integer.toString(abchanneltotal)));
            try {
                channeltable.put(abchannelput);
            } catch (InterruptedIOException e) {
                //
            } catch (RetriesExhaustedWithDetailsException e) {
                //
            }
//            String[] costs;
//            if (!productresult.isEmpty()) {
//                productfee = Bytes.toString(productresult.getValue(Bytes.toBytes("cf"),
//                        Bytes.toBytes("fee")));
//                costs = productfee.split(",");
//                int abproduct = Integer.parseInt(costs[0]) + Integer.parseInt(cost);
//                int producttotal = Integer.parseInt(costs[1]) + Integer.parseInt(cost);
//                productput.add(Bytes.toBytes("cf"), Bytes.toBytes("fee"),
//                        Bytes.toBytes(Integer.toString(abproduct) + "," + Integer.toString(producttotal)));
//            } else {
//                productput.add(Bytes.toBytes("cf"), Bytes.toBytes("fee"),
//                        Bytes.toBytes(cost + "," + cost));
//            }
//            try {
//                producttable.put(productput);
//            } catch (InterruptedIOException e) {
//                //
//            } catch (RetriesExhaustedWithDetailsException e) {
//                //
//            }
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }

}
