package bolts;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import conf.StormConf;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;
import util.FName;
import util.StatisticMap;
import util.TimeUtil;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 对异常订单进行回溯和汇总
 * Created by linwanying on 2016/5/19.
 */
public class BackTrackBolt extends BaseRichBolt {
    static Logger log = Logger.getLogger(BackTrackBolt.class);

    private Connection conn = null;
    private String orderRuletable = StormConf.realTimeOutputTable;
    private String datawarehouseTable = StormConf.dataWarehouseTable;
    private String channelcodeTable = StormConf.channelCodesTable;
    private HTable ordertable;
    private ConcurrentHashMap<String, StatisticMap> statisticMap = null;

    @SuppressWarnings("rawtypes")
    public void prepare(Map config, TopologyContext context, OutputCollector collector) {
        statisticMap = new ConcurrentHashMap<String, StatisticMap>();
        String url = StormConf.URL;
        String user = StormConf.USER;
        String password = StormConf.PASSWORD;
        String driverName = StormConf.DRIVERNAME;

        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "10.1.69.170");
        conf.set("hbase.zookeeper.property.clientPort", "2182");
        try {
            ordertable = new HTable(conf, Bytes.toBytes("order_info"));
        } catch (IOException e) {
            //DO NOTHING
        }

        try {
            Class.forName(driverName);
            // 获取数据库连接
            conn = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void orderBackTrack(String msisdn, String channelcode, int rule, long recordTime,
                               long threshold, Result result) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");//设置日期格式
        Date today = null;
        try {
            today = new Date(df.parse("2015/06/06").getTime());
        } catch (ParseException e) {
            //DO NOTHING
        }
        String info = Bytes.toString(result.getValue(
                Bytes.toBytes("cf"), Bytes.toBytes("info")));
        String[] data = info.split("\\|");
        List<String> list = new ArrayList<String>();
        for (String s : data) {
            list.add(s);
        }
        StringBuffer sb = new StringBuffer();
        StringBuffer insertSQL = new StringBuffer(), deleteSQL = new StringBuffer();
        insertSQL.append("insert into ").append(orderRuletable)
                .append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        deleteSQL.append("delete from ").append(orderRuletable).append(" where msisdn=? and sessionid=?")
                .append(" and channelcode=? and bookid=? and productid=? and realfee=? and ordertime=?");
        for (int i = 0; i < list.size(); ++i) {
            String cur = list.get(i);
            String[] curdata = cur.split(",");
            String curtype = curdata[0];  //当前查询数据的ordertype
            String curbookid = curdata[1];
            String curchapterid = curdata[2];
            String curfee = curdata[3];//fee
            String curtime = curdata[4];//recordtime
            String cursession = curdata[5];
            String curproduct = curdata[6];
            String curprovince = curdata[7];
            StringBuffer currule = new StringBuffer(curdata[8]);
            String contentID,contentType,statisticKey;
            String stime = TimeUtil.getTimeString(recordTime);
            int realfee = Integer.parseInt(curfee);
            long timelag = recordTime - Long.parseLong(curtime);
            if (list.size() != 0 && timelag <= threshold && timelag >= 0) {
                if (currule.charAt(rule - 1) == '1') {
                    if (!(curtype.equals("4") || curtype.equals("5"))) {
                        contentID = curbookid;
                        contentType = "3";
                    } else {
                        contentID = curproduct;
                        if (curtype.equals("5")) {
                            contentType = "2";
                        } else {
                            contentType = "1";
                        }
                    }
                    statisticKey = contentID + "," + contentType + "," + channelcode + "," + curprovince;
                    statisticMap.get(statisticKey).put(Integer.toString(rule), realfee);
                    currule.setCharAt(rule - 1, '0');
                    list.set(i, curtype + "," + curbookid + "," + curchapterid + "," + curfee + ","
                            + curtime + "," + cursession + "," + curproduct + "," + curprovince + "," + currule);
                    PreparedStatement deleteps = null;
                    PreparedStatement insertps = null;
                    try {
                        deleteps = conn.prepareStatement(deleteSQL.toString());
                        deleteps.setString(1, msisdn);
                        deleteps.setString(2, cursession);
                        deleteps.setString(3, channelcode);
                        deleteps.setString(4, curbookid);
                        deleteps.setString(5, curproduct);
                        deleteps.setInt(6, realfee);
                        deleteps.setString(7, stime);
                        deleteps.execute();

                        insertps = conn.prepareStatement(insertSQL.toString());
                        insertps.setDate(1, today);
                        insertps.setString(2, msisdn);
                        insertps.setString(3, cursession);
                        insertps.setString(4, channelcode);
                        insertps.setString(5, curbookid);
                        insertps.setString(6, curproduct);
                        insertps.setInt(7, realfee);
                        insertps.setString(8, currule.charAt(0) == '0' ? "0" : "1");
                        insertps.setString(9, currule.charAt(1) == '0' ? "0" : "1");
                        insertps.setString(10, currule.charAt(2) == '0' ? "0" : "1");
                        insertps.setString(11, currule.charAt(3) == '0' ? "0" : "1");
                        insertps.setString(12, currule.charAt(4) == '0' ? "0" : "1");
                        insertps.setString(13, currule.charAt(5) == '0' ? "0" : "1");
                        insertps.setString(14, currule.charAt(6) == '0' ? "0" : "1");
                        insertps.setString(15, currule.charAt(7) == '0' ? "0" : "1");
                        insertps.setString(16, currule.charAt(8) == '0' ? "0" : "1");
                        insertps.setString(17, currule.charAt(9) == '0' ? "0" : "1");
                        insertps.setString(18, currule.charAt(10) == '0' ? "0" : "1");
                        insertps.setString(19, currule.charAt(11) == '0' ? "0" : "1");
                        insertps.setString(20, stime);
                        insertps.execute();

                        log.info("insert oracle:" + msisdn + "," + cursession + "," + stime);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (deleteps != null) {
                                deleteps.close();
                            }
                            if (insertps != null) {
                                insertps.close();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
            if (i == list.size() - 1) {
                sb.append(list.get(i));
            } else {
                sb.append(list.get(i) + "|");
            }
        }
        Put put = new Put(Bytes.toBytes(msisdn + "_" + channelcode));
        put.add(Bytes.toBytes("cf"), Bytes.toBytes("info"),
                Bytes.toBytes(sb.toString()));
        try {
            ordertable.put(put);
        } catch (InterruptedIOException e) {
            e.printStackTrace();
        } catch (RetriesExhaustedWithDetailsException e) {
            e.printStackTrace();
        }
    }

    public void MsisdnChannelBT(String msisdn, String channelcode, long recordTime, int rule) {
        long threshold;
        String msisdn_channelcode = msisdn + "_" + channelcode;
        if (rule == 4 || rule == 5) {
            threshold = 86400000;
        } else {
            threshold = 3600000;
        }
        Get get = new Get(Bytes.toBytes(msisdn_channelcode));
        Result result = null;
        try {
            result = ordertable.get(get);
        } catch (IOException e) {
            //
        }
        if (!result.isEmpty()) {
            orderBackTrack(msisdn, channelcode, rule, recordTime, threshold, result);
        }
    }

    public void MsisdnBT(String msisdn, String channelCode, long recordTime, int rule) {
        Scan scan = new Scan();
        scan.setStartRow(Bytes.toBytes(msisdn + "_0000"));
        scan.setStopRow(Bytes.toBytes(msisdn + "_:"));

        ResultScanner rs = null;
        try {
            rs = ordertable.getScanner(scan);
        } catch (IOException e) {
            //
        }
        for (Result r : rs) {
            orderBackTrack(msisdn, channelCode, rule, recordTime, 3600000, r);
        }
    }

    public void execute(Tuple tuple) {
        long recordTime = tuple.getLongByField(FName.RECORDTIME.name());
        String msisdn = tuple.getStringByField(FName.MSISDN.name());
        String orderType = tuple.getStringByField(FName.ORDERTYPE.name());
        String productID = tuple.getStringByField(FName.PRODUCTID.name());
        String bookID = tuple.getStringByField(FName.BOOKID.name());
        String channelCode = tuple.getStringByField(FName.CHANNELCODE.name());
        String cost = tuple.getStringByField(FName.COST.name());
        String sessionId = tuple.getStringByField(FName.SESSIONID.name());
        String provinceID = tuple.getStringByField(FName.PROVINCEID.name());
        String rule = tuple.getStringByField(FName.RULE.name());
        String contentID,contentType,statisticKey;
        int realfee = Integer.parseInt(cost);
        if (orderType.equals("4")) {
            contentID = productID;
            contentType = "1";
        } else if (orderType.equals("5")) {
            contentID = productID;
            contentType = "2";
        } else {
            contentID = bookID;
            contentType = "3";
        }
        statisticKey = contentID + "," + contentType + "," + channelCode + "," + provinceID;
        if (!statisticMap.containsKey(statisticKey)) {
            statisticMap.get(statisticKey).setTotal(realfee);
        } else {
            ConcurrentHashMap<String, Integer> tmp = new ConcurrentHashMap<String, Integer>();
            StatisticMap smap = new StatisticMap(1, realfee, tmp);
            statisticMap.put(statisticKey,smap);
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");//设置日期格式
        Date today = null;
        try {
            today = new Date(df.parse("2015/06/06").getTime());
        } catch (ParseException e) {
            //DO NOTHING
        }
        StringBuffer insertSQL = new StringBuffer();
        insertSQL.append("insert into ").append(orderRuletable)
                .append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        // 创建该连接下的PreparedStatement对象
        PreparedStatement pstmt = null;
        try {

            pstmt = conn.prepareStatement(insertSQL.toString());
            pstmt.setDate(1, today);
            pstmt.setString(2, msisdn);
            pstmt.setString(3, sessionId);
            pstmt.setString(4, channelCode);
            pstmt.setString(5, bookID);
            pstmt.setString(6, productID);
            pstmt.setInt(7, Integer.parseInt(cost));
            pstmt.setString(8, rule.charAt(0) == '0' ? "0" : "1");
            pstmt.setString(9, rule.charAt(1) == '0' ? "0" : "1");
            pstmt.setString(10, rule.charAt(2) == '0' ? "0" : "1");
            pstmt.setString(11, rule.charAt(3) == '0' ? "0" : "1");
            pstmt.setString(12, rule.charAt(4) == '0' ? "0" : "1");
            pstmt.setString(13, rule.charAt(5) == '0' ? "0" : "1");
            pstmt.setString(14, rule.charAt(6) == '0' ? "0" : "1");
            pstmt.setString(15, rule.charAt(7) == '0' ? "0" : "1");
            pstmt.setString(16, rule.charAt(8) == '0' ? "0" : "1");
            pstmt.setString(17, rule.charAt(9) == '0' ? "0" : "1");
            pstmt.setString(18, rule.charAt(10) == '0' ? "0" : "1");
            pstmt.setString(19, rule.charAt(11) == '0' ? "0" : "1");
            pstmt.setString(20, TimeUtil.getTimeString(recordTime));

            pstmt.execute();

            if (rule.charAt(0) == '0') {
                MsisdnChannelBT(msisdn, channelCode, recordTime, 1);
            }
            if (rule.charAt(1) == '0') {
                MsisdnChannelBT(msisdn, channelCode, recordTime, 2);
            }
            if (rule.charAt(2) == '0') {
                MsisdnChannelBT(msisdn, channelCode, recordTime, 3);
            }
            if (rule.charAt(3) == '0') {
                MsisdnChannelBT(msisdn, channelCode, recordTime, 4);
            }
            if (rule.charAt(4) == '0') {
                MsisdnChannelBT(msisdn, channelCode, recordTime, 5);
            }
            if (rule.charAt(5) == '0') {
                MsisdnChannelBT(msisdn, channelCode, recordTime, 6);
            }
            if (rule.charAt(6) == '0') {
                MsisdnChannelBT(msisdn, channelCode, recordTime, 7);
            }
            if (rule.charAt(7) == '0') {
                MsisdnChannelBT(msisdn, channelCode, recordTime, 8);
            }
            if (rule.charAt(8) == '0') {
                MsisdnBT(msisdn, channelCode, recordTime, 9);
            }
            if (rule.charAt(9) == '0') {
                MsisdnBT(msisdn, channelCode, recordTime, 10);
            }
            if (rule.charAt(10) == '0') {
                MsisdnBT(msisdn, channelCode, recordTime, 11);
            }
            if (rule.charAt(11) == '0') {
                statisticMap.get(statisticKey).put("12", realfee);
            }
            int count = statisticMap.get(statisticKey).getCount();
            if (count >= 100) {
                LinkedBlockingQueue<String> rules = statisticMap.get(statisticKey).getRules();
                Iterator<String> it = rules.iterator();
                while (it.hasNext()) {
                    String tmprule = it.next();
                    int abfee = statisticMap.get(statisticKey).getStmap().get(tmprule);
                    int total = statisticMap.get(statisticKey).getTotal();
                    double abrate = abfee/total;
                    PreparedStatement dataps;
                    PreparedStatement selectps;
                    ResultSet rs;
                    StringBuffer insertData = new StringBuffer();
                    StringBuffer selectSQL = new StringBuffer();
                    selectSQL.append("select FIRST_CHANNEL_ID,SECOND_CHANNEL_ID,THIRD_CHANNEL_ID")
                            .append(" from ").append(channelcodeTable).append(" where parameter_id=?");
                    selectps = conn.prepareStatement(selectSQL.toString());
                    selectps.setString(1, channelCode);
                    rs = selectps.executeQuery();

                    String first_channel_id = null, second_channel_id = null, third_channel_id = null;
                    while (rs.next()) {
                        first_channel_id = rs.getString("FIRST_CHANNEL_ID");
                        second_channel_id = rs.getString("SECOND_CHANNEL_ID");
                        third_channel_id = rs.getString("THIRD_CHANNEL_ID");
                    }
                    String record_day = TimeUtil.getToday();
                    String load_time = TimeUtil.getTodayDetail();
                    insertData.append("insert into ").append(datawarehouseTable)
                            .append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    dataps = conn.prepareStatement(insertData.toString());
                    dataps.setString(1, record_day);
                    dataps.setString(2, provinceID);
                    dataps.setString(3, first_channel_id);
                    dataps.setString(4, second_channel_id);
                    dataps.setString(5, third_channel_id);
                    dataps.setString(6, contentID);
                    dataps.setString(7, channelCode);
                    dataps.setInt(8, abfee);
                    dataps.setInt(9, total);
                    dataps.setDouble(10, abrate);
                    dataps.setString(11, contentType);
                    dataps.setString(12, tmprule);
                    dataps.setString(13, load_time);
                    dataps.execute();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
//                if (conn != null) {
//                    conn.close();
//                }
            } catch (SQLException e) {
                //
            }

        }
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }

}
