package main;

import backtype.storm.Config;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import bolt.OrderSplit;
import bolts.BackTrackBolt;
import bolts.CacheBolt;
import bolts.RuleBolt;
import bolts.StatisticsBolt;
import conf.StormConf;
import org.apache.log4j.Logger;
import storm.kafka.*;
import util.FName;
import util.StreamId;

/**异常订购topo
 *
 * Created by linwanying on 2016/5/4.
 */
public class AbnormalOrderTopo {
    static Logger log = Logger.getLogger(AbnormalOrderTopo.class);

    public static void main(String[] args) throws AlreadyAliveException, InvalidTopologyException {
        log.info("Start topology.");

        String zkCfg = StormConf.ZKCFG;
        String[] topics = StormConf.TOPIC;
        String zkRoot = StormConf.ZKROOT;
        BrokerHosts brokerHosts = new ZkHosts(zkCfg);

        //订购话单
        SpoutConfig orderSpoutConfigTopic = new SpoutConfig(brokerHosts, topics[1], zkRoot, "order");
        orderSpoutConfigTopic.scheme = new SchemeAsMultiScheme(new StringScheme());
        orderSpoutConfigTopic.forceFromStart = false;//只接收实时数据
        orderSpoutConfigTopic.socketTimeoutMs = 60000;

        Config conf = new Config();
        TopologyBuilder builder = new TopologyBuilder();

        //订购话单发射、分词bolt
        builder.setSpout(StreamId.report_cdr.name(), new KafkaSpout(orderSpoutConfigTopic), 3);
        builder.setBolt(StreamId.OrderSplit.name(), new OrderSplit(), 3)
                .shuffleGrouping(StreamId.report_cdr.name());
        //订购话单缓存bolt
        builder.setBolt(StreamId.CacheBolt.name(), new CacheBolt(), 1)
                .fieldsGrouping(StreamId.OrderSplit.name(), StreamId.ORDERDATA.name(),new Fields(FName.MSISDN.name()));
        //订购话单规则判断bolt
        builder.setBolt(StreamId.RuleBolt.name(), new RuleBolt(), 3)
                .fieldsGrouping(StreamId.CacheBolt.name(), StreamId.DATASTREAM.name(), new Fields(FName.MSISDN.name()));
        //订购话单统计bolt
        builder.setBolt(StreamId.BackTrackBolt.name(), new BackTrackBolt(), 3)
                .fieldsGrouping(StreamId.RuleBolt.name(), StreamId.DATASTREAM2.name(), new Fields(FName.MSISDN.name()));

        conf.setNumWorkers(3);
        conf.setNumAckers(0);
        conf.setMaxSpoutPending(100000);
        conf.setMessageTimeoutSecs(60000);
        conf.put(Config.TOPOLOGY_RECEIVER_BUFFER_SIZE, 8);
        conf.put(Config.TOPOLOGY_TRANSFER_BUFFER_SIZE, 32);
        conf.put(Config.TOPOLOGY_EXECUTOR_RECEIVE_BUFFER_SIZE, 16384);
        conf.put(Config.TOPOLOGY_EXECUTOR_SEND_BUFFER_SIZE, 16384);
        StormSubmitter.submitTopology(StormConf.ORDERTOPONAME, conf, builder.createTopology());
    }
}


