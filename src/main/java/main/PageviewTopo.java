package main;

import backtype.storm.Config;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import bolt.PageviewSplit;
import bolts.PvBolt;
import conf.StormConf;
import org.apache.log4j.Logger;
import storm.kafka.*;
import util.FName;
import util.StreamId;
/**
 * Created by liwanying on 2016/4/13.
 */
public class PageviewTopo {

    static Logger log = Logger.getLogger(PageviewTopo.class);

    public static void main(String[] args) throws AlreadyAliveException, InvalidTopologyException {
        log.info("Start topology.");

        String zkCfg = StormConf.ZKCFG;
        String[] topics = StormConf.TOPIC;
        String zkRoot = StormConf.ZKROOT;
        BrokerHosts brokerHosts = new ZkHosts(zkCfg);

        //浏览话单
        SpoutConfig pageViewSpoutConfigTopic = new SpoutConfig(brokerHosts, topics[0], zkRoot, "pageview");
        pageViewSpoutConfigTopic.scheme = new SchemeAsMultiScheme(new StringScheme());
        pageViewSpoutConfigTopic.forceFromStart = false;//只接收实时数据
        pageViewSpoutConfigTopic.socketTimeoutMs = 60000;

        Config conf = new Config();
        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout(StreamId.Portal_Pageview.name(), new KafkaSpout(pageViewSpoutConfigTopic), 3);
        builder.setBolt(StreamId.PageViewSplit.name(), new PageviewSplit(), 3)
                .shuffleGrouping(StreamId.Portal_Pageview.name());
        builder.setBolt(StreamId.PvBolt.name(), new PvBolt(), 3)
                .fieldsGrouping(StreamId.PageViewSplit.name(), StreamId.BROWSEDATA.name(), new Fields(FName.MSISDN.name()));

        conf.setNumWorkers(3);
        conf.setNumAckers(0);
        conf.setMaxSpoutPending(100000);
        conf.setMessageTimeoutSecs(60000);
        conf.put(Config.TOPOLOGY_RECEIVER_BUFFER_SIZE,             8);
        conf.put(Config.TOPOLOGY_TRANSFER_BUFFER_SIZE,            32);
        conf.put(Config.TOPOLOGY_EXECUTOR_RECEIVE_BUFFER_SIZE, 16384);
        conf.put(Config.TOPOLOGY_EXECUTOR_SEND_BUFFER_SIZE,    16384);
        StormSubmitter.submitTopology(StormConf.PVTOPONAME, conf, builder.createTopology());
    }

}
