package util;


import java.util.Iterator;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by linwanying on 2016/4/6.
 */
public class TimeCacheList<T extends TimeBaseItem> {
    private LinkedBlockingDeque<T> data;
    private long threshold;

    public TimeCacheList(long threshold) {
        this.data = new LinkedBlockingDeque<T>();
        this.threshold = threshold;
    }

    /**
     * 清除表中过期的数据。
     * @param currentTime 时间(millis)
     */
    private void clearTimeOutData(String info, long currentTime) {
        Iterator<T> it = data.iterator();
        while (it.hasNext()) {
            TimeBaseItem tmp = it.next();
            if (tmp.getRuleInfo().equals(info) ||
                    Math.abs(tmp.getRecordTime() - currentTime) > this.threshold) {
                it.remove();
            }
        }
    }

    /**
     *
     * @param tb
     */
    public void put(T tb) {
        this.clearTimeOutData(tb.getRuleInfo(), tb.getRecordTime());
        try {
            data.putLast(tb);
        } catch (InterruptedException e) {
            //do nothing
        }
    }

    public int size() {
        return data.size();
    }
}
