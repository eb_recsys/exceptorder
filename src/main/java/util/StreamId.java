package util;

import bolts.CacheBolt;

public enum StreamId {
	TOPIC1, TOPIC2, BOLT1, BOLT2, ORDERDATA, BROWSEDATA, DATASTREAM, ABNORMALDATASTREAM,
    report_cdr, Portal_Pageview, OrderSplit, PageViewSplit, MessageBufferBolt, StatisticsBolt, RealTimeOutputBolt,
    DataWarehouseBolt, DATASTREAM2, StreamId, ABNORMALDATASTREAM2, PvBolt, PageViewTopo, CacheBolt, RuleBolt,
    BackTrackBolt,
}
