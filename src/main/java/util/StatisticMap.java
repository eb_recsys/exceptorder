package util;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 按渠道，营销id，内容id，内容类型和省份统计异常费用。
 * Created by linwanying on 2016/5/24.
 */
public class StatisticMap {
    private int count = 0;
    private int total = 0;
    private LinkedBlockingQueue<String> rules = new LinkedBlockingQueue<String>();
    private ConcurrentHashMap<String, Integer> stmap = new ConcurrentHashMap<String, Integer>();

    public StatisticMap(int count, int total, ConcurrentHashMap<String, Integer> stmap) {
        this.count = count;
        this.total = total;
        this.stmap = stmap;
    }

    public void put(String rule, int realfee) {
        if (!rule.isEmpty()) {
            if (stmap.containsKey(rule)) {
                stmap.put(rule, realfee + stmap.get(rule));
            } else {
                stmap.put(rule, realfee);
                try {
                    rules.put(rule);
                } catch (InterruptedException e) {
                    //
                }
            }
        }
        count += 1;
    }

    public void setTotal(int realfee) {
        total += realfee;
        count += 1;
    }

    public int getCount() {
        return count;
    }

    public int getTotal() {
        return total;
    }

    public LinkedBlockingQueue<String> getRules() {
        return rules;
    }

    public ConcurrentHashMap<String, Integer> getStmap() {
        return stmap;
    }
}
