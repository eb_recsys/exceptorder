package util;

/**
 * Created by linwanying on 2016/4/11.
 */
public class TimeBaseItem {
    private Long recordTime;
    private String ruleInfo;

    public TimeBaseItem(String ruleInfo, Long recordTime) {
        this.ruleInfo = ruleInfo;
        this.recordTime = recordTime;
    }

    public Long getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Long recordTime) {
        this.recordTime = recordTime;
    }

    public String getRuleInfo() {
        return ruleInfo;
    }

    public void setRuleInfo(String ruleInfo) {
        this.ruleInfo = ruleInfo.trim();
    }
}
