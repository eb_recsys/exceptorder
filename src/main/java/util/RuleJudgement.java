package util;

import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 规则1~12判断
 * Created by linwanying on 2016/4/26.
 */
public class RuleJudgement {
    /**
     * @param msisdn_bookid 用户id_书本id/章节id
     * @param recordtime    记录时间
     * @param pvtable       hbase的pv_info表格
     * @return 计算pv点击数
     */
    public static int pvCount(String msisdn_bookid, HTable pvtable, long recordtime, long start, long end) {
        int count = 0;
        String info;
        String[] data1;
        Get get = new Get(Bytes.toBytes(msisdn_bookid));
        Result result = null;
        try {
            result = pvtable.get(get);
        } catch (IOException e) {
            //TO DO
        }

        if (!result.isEmpty()) {
            info = Bytes.toString(result.getValue(
                    Bytes.toBytes("cf"), Bytes.toBytes("info")));
            data1 = info.split("\\|");
            List<String> list = new ArrayList<String>();
            for (String s : data1) {
                list.add(s);
            }
            Iterator<String> it = list.iterator();
            while (it.hasNext()) {
                String cur = it.next();
                Long timelag = recordtime - Long.parseLong(cur);
                if (list.size() != 0 && (timelag <= start && timelag >= end)) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * 规则4,9,10,11的判断方法，符合异常规则返回true
     *
     * @param orderItem 订单数据
     * @param ruleType  判断的规则类型
     * @return map中同一个key下的sessionid等个数
     */
    public static int rule4_9_10_11(ConcurrentHashMap<String, TimeCacheList> map, TimeBaseItem timeBaseItem,
                                    OrderItem orderItem, String ruleType) {
        long threshold;
        String msisdn = orderItem.getMsisdn();
        if (ruleType.equals("4")) {
            threshold = 86400000;
        } else {
            threshold = 3600000;
        }
        if (map.containsKey(msisdn)) {
            map.get(msisdn).put(timeBaseItem);
        } else {
            TimeCacheList timeCacheList = new TimeCacheList(threshold);
            timeCacheList.put(timeBaseItem);
            map.put(msisdn, timeCacheList);
        }
        return map.get(msisdn).size();
    }

    /**
     * 规则5的判断方法
     *
     * @param orderItem  订单数据
     * @param ordertable hbase中的order_info表格
     * @return 是否符合规则
     */
    public static boolean rule5(OrderItem orderItem, HTable ordertable) {
        boolean flag = false;
        int fee = 0;
        String info;
        String[] data1;
        long recordTime = orderItem.getRecordTime();
        String msisdn_channelcode = orderItem.getMsisdn() + "_" + orderItem.getChannelCode();

        if (orderItem.getOrderType().equals("1")) {
            fee = Integer.parseInt(orderItem.getCost());
        }
        Get get = new Get(Bytes.toBytes(msisdn_channelcode));
        Result result = null;
        try {
            result = ordertable.get(get);
        } catch (IOException e) {
            //TO DO
        }
        if (!result.isEmpty()) {
            info = Bytes.toString(result.getValue(
                    Bytes.toBytes("cf"), Bytes.toBytes("info")));
            data1 = info.split("\\|");
            List<String> list = new ArrayList<String>();
            for (String s : data1) {
                list.add(s);
            }
            Iterator<String> it = list.iterator();
            while (it.hasNext()) {
                String cur = it.next();
                String curtype = cur.split(",")[0];  //当前查询数据的ordertype
                int curfee = Integer.parseInt(cur.split(",")[3]);//fee
                long curtime = Long.parseLong(cur.split(",")[4]);//recordtime
                if (list.size() != 0 && recordTime - curtime < 86400000) {
                    if (curtype.equals("1")) {
                        fee += curfee;
                    }
                }
            }
        }
        if (fee > 10 * 100) {
            flag = true;
        }
        return flag;
    }

    /**
     * 规则6判断方法，异常则返回true
     *
     * @param msisdn     用户id
     * @param recordtime 记录时间
     * @param ordertable hbase中的order_info表格
     * @return 是否符合规则
     */
    public static boolean rule6(String msisdn, String ordertype, long recordtime, HTable ordertable) {
        boolean flag = false;
        int count = 0;
        if (ordertype.equals("4")) {
            count = 1;
        }
        String[] data1 = null;
        Scan scan = new Scan();
        scan.setStartRow(Bytes.toBytes(msisdn + "_0000"));
        scan.setStopRow(Bytes.toBytes(msisdn + "_:"));

        ResultScanner rs = null;
        try {
            rs = ordertable.getScanner(scan);
        } catch (IOException e) {
            //DO NOTHING
        }
        String info = null;
        for (Result r : rs) {
            info = Bytes.toString(r.getValue(
                    Bytes.toBytes("cf"), Bytes.toBytes("info")));
            data1 = info.split("\\|");
            List<String> list = new ArrayList<String>();
            for (String s : data1) {
                list.add(s);
            }
            Iterator<String> it = list.iterator();
            while (it.hasNext()) {
                String cur = it.next();
                String curtype = cur.split(",")[0];  //当前查询数据的ordertype
                long curtime = Long.parseLong(cur.split(",")[4]);//recordtime
                if (list.size() != 0 && recordtime - curtime < 180000) {
                    if (curtype.equals("4")) {
                        count++;
                    }
                }
            }
        }
        if (count >= 2) {
            flag = true;
        }
        return flag;
    }

    /**
     * 规则7,8的判断，异常则返回true
     *
     * @param msisdn     用户id
     * @param recordtime 记录时间
     * @param pvtable    hbase中的pv_info表格
     * @param ordertable hbase中的order_info表格
     * @return 是否符合规则
     */
    public static boolean rule7_8(long recordtime, String msisdn, String ruletype, HTable pvtable, HTable ordertable) {
        boolean flag = false, rulejudge;
        int bookcount = 0, pvcount = 0, book = 0, pv = 0;
        String[] data1 = null;
        String msisdn_bookid;
        if (ruletype.equals("7")) {
            pv = 5;
            book = 2;
        } else {
            pv = 2;
            book = 10;
        }
        Scan scan = new Scan();
        scan.setStartRow(Bytes.toBytes(msisdn + "_0000"));
        scan.setStopRow(Bytes.toBytes(msisdn + "_:"));

        ResultScanner rs = null;
        try {
            rs = ordertable.getScanner(scan);
        } catch (IOException e) {
            //DO NOTHING
        }
        String info = null;
        for (Result r : rs) {
            info = Bytes.toString(r.getValue(
                    Bytes.toBytes("cf"), Bytes.toBytes("info")));
            data1 = info.split("\\|");
            List<String> list = new ArrayList<String>();
            for (String s : data1) {
                list.add(s);
            }
            Iterator<String> it = list.iterator();
            while (it.hasNext()) {
                String cur = it.next();
                String curtype = cur.split(",")[0];  //当前查询数据的ordertype
                String curbookid = cur.split(",")[1];            //bookid
                String curchapter = cur.split(",")[2];           //chapterid
                if (ruletype.equals("7")) {
                    rulejudge = (curtype.equals("2") && curchapter.isEmpty()) || curtype.equals("1");
                    msisdn_bookid = msisdn + "_" + curbookid;
                } else {
                    rulejudge = curtype.equals("2");
                    msisdn_bookid = msisdn + "_" + curchapter;
                }
                long curtime = Long.parseLong(cur.split(",")[4]);  //当前查询数据的recordtime
                if (list.size() != 0 && recordtime - curtime < 300000) {
                    if (rulejudge) {
                        bookcount++;
                        pvcount += RuleJudgement.pvCount(msisdn_bookid, pvtable, curtime, 300000, 0);
                    }
                }
            }
        }
        if (bookcount >= book && pvcount <= pv * bookcount) {
            flag = true;
        }
        return flag;
    }
}
