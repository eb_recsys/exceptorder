package util;

/**
 * Created by hp on 2016/4/11.
 */
public class OrderItem {
    String msisdn;
    Long recordTime;
    String terminal;
    String platform;
    String orderType;
    String productID;
    String bookID;
    String chapterID;
    String channelCode;
    String cost;
    String provinceId;
    String wapIp;
    String sessionId;
    String promotionid;

    public OrderItem(String msisdn,Long recordTime,String terminal,String platform,String orderType,String productID,
                     String bookID,String chapterID,String channelCode,String cost,String provinceId,String wapIp,
                     String sessionId,String promotionid){
        this.recordTime = recordTime;
        this.msisdn = msisdn;
        this.bookID = bookID;
        this.chapterID = chapterID;
        this.channelCode = channelCode;
        this.cost = cost;
        this.orderType = orderType;
        this.platform = platform;
        this.productID = productID;
        this.provinceId = provinceId;
        this.terminal = terminal;
        this.wapIp = wapIp;
        this.sessionId =sessionId;
        this.promotionid = promotionid;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Long getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Long recordTime) {
        this.recordTime = recordTime;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getBookID() {
        return bookID;
    }

    public void setBookID(String bookID) {
        this.bookID = bookID;
    }

    public String getChapterID() {
        return chapterID;
    }

    public void setChapterID(String chapterID) {
        this.chapterID = chapterID;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getWapIp() {
        return wapIp;
    }

    public void setWapIp(String wapIp) {
        this.wapIp = wapIp;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getPromotionid() {
        return promotionid;
    }

    public void setPromotionid(String promotionid) {
        this.promotionid = promotionid;
    }
}
