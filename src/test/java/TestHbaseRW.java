
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

public class TestHbaseRW {

	public static void main(String[] args) throws IOException {
		Configuration conf = HBaseConfiguration.create();
		conf.set("hbase.zookeeper.quorum", "10.1.69.170");
		conf.set("hbase.zookeeper.property.clientPort", "2182");

		HTable table = new HTable(conf, Bytes.toBytes("unify_base_score"));
		
		Put put = new Put(Bytes.toBytes("100001"));
		put.add(Bytes.toBytes("cf"), Bytes.toBytes("c"),
				Bytes.toBytes("0.9"));
		table.put(put);
		
		put = new Put(Bytes.toBytes("100001"));
		put.add(Bytes.toBytes("cf"), Bytes.toBytes("class"),
				Bytes.toBytes("2"));
		table.put(put);
		
		put = new Put(Bytes.toBytes("100001"));
		put.add(Bytes.toBytes("cf"), Bytes.toBytes("orderid"),
				Bytes.toBytes("0"));
		table.put(put);
		
		put = new Put(Bytes.toBytes("100001"));
		put.add(Bytes.toBytes("cf"), Bytes.toBytes("seriesid"),
				Bytes.toBytes("0"));
		table.put(put);

		String rowKey = "100001";
		Get get = new Get(Bytes.toBytes(rowKey));
		Result result = null;
		result = table.get(get);
		String score = null;
		String category = null;
		if (!result.isEmpty()) {
			score = Bytes.toString(result.getValue(
					Bytes.toBytes("cf"), Bytes.toBytes("c")));
			category = Bytes.toString(result.getValue(
					Bytes.toBytes("cf"), Bytes.toBytes("class")));
		}
		System.out.println(score + "   " + category);
		
	}
}
